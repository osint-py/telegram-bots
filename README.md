# Telegram-bots

This is a public respository including a wiki.

1. The respository will contain an example of a Simple Telegram Bot in Python, comparable to the 
[Simple Twitter bot in Python that tweets daily a joke](https://gitlab.com/osint-py/SimpleDailyTwitPyBot) (SimpleDailyTwitPyBot).
1. The wiki will contain a lot of information on Telegram scripts, wrappers, documentation etc.

_Note_: 
For now the wiki also contains links to other #osint information.

Feel free to make adjustments to the code and/or wiki.

![This is the way](https://media.giphy.com/media/cIhxXR4XnFEfshBQQj/giphy.gif)


# Bot raison d'exister
* Goal 1: post something in a group
* Goal 2: retrieve complete message history of a group (backup)
* Limititation: This bot will not interact with other users.

**Table of Content**

[[_TOC_]]



# Prerequisites
1. You need an email address/addresses for
    1. Gitlab account
1. Programming Environment
1. Skills
    1. You have completed a Python introduction course.
    1. You have completed a Git or Gitlab introduction course.


# Step 2 - Telegram
# Step 3 - Create Python code
## Create a python project directory
## Clone your respository from Gitlab to your machine
## Create the python environment of your Bot



# License
Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)

```
#   You are free to:
#   Share — copy and redistribute the material in any medium or format.
#   Adapt — remix, transform, and build upon the material for any purpose, even commercially.
#   The licensor cannot revoke these freedoms as long as you follow the license terms.
#    
#    You can copy, modify, distribute and perform the work,  
#    even for commercial purposes, all without asking permission.
#    
#    Under the following terms:
#    Attribution — You must give appropriate credit, provide a link to the license, 
#                  and indicate if changes were made. You may do so in any reasonable manner, 
#                  but not in any way that suggests the licensor endorses you or your use.
#    ShareAlike — If you remix, transform, or build upon the material, 
#                 you must distribute your contributions under the same license as the original.
#    No additional restrictions — You may not apply legal terms or technological measures that 
#                 legally restrict others from doing anything the license permits.
#    
#    For more see the file 'LICENSE' for copying permission.
```
